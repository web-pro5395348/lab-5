import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import type { ReceiptItem } from '@/types/ReceiptItem'

export const useReceiptStore = defineStore('receipt', () => {
  const receiptItems = ref<ReceiptItem[]>([])
  const addReceiptItem = (product: Product) => {
    // the ? sign is called optional chaining to prevent error
    const index = receiptItems.value.findIndex((item) => item.product?.id === product.id)

    if (index >= 0) {
      receiptItems.value[index].unit++
      return
    } else {
      const newReceipt: ReceiptItem = {
        id: -1,
        name: product.name,
        price: product.price,
        unit: 1,
        productId: product.id,
        product: product
      }
      receiptItems.value.push(newReceipt)
    }

  }
  const removeReceiptItem = (receiptItem: ReceiptItem) => {
    // the line below is called "callback function" to find index of array
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
  }
  function inc(item: ReceiptItem) {
    item.unit++
  }
  const dec = (item: ReceiptItem) => {
    if (item.unit === 1) {
      removeReceiptItem(item)
      return
    }
    item.unit--
  }

  return {
    receiptItems,
    addReceiptItem, removeReceiptItem, inc, dec
  }
})
